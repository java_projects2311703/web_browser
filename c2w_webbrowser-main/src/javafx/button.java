import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

 class JourneyToDestinationUI extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Journey to Destination");

        // Create ComboBox for "From" button
        ComboBox<String> fromComboBox = new ComboBox<>();
        fromComboBox.getItems().addAll("City A", "City B", "City C", "City D");
        Button fromButton = new Button("From");
        fromButton.setOnAction(e -> {
            String selectedCity = fromComboBox.getValue();
            System.out.println("From: " + selectedCity);
        });

        // Create ComboBox for "To" button
        ComboBox<String> toComboBox = new ComboBox<>();
        toComboBox.getItems().addAll("City X", "City Y", "City Z", "City W");
        Button toButton = new Button("To");
        toButton.setOnAction(e -> {
            String selectedCity = toComboBox.getValue();
            System.out.println("To: " + selectedCity);
        });

        // Create layout
        HBox root = new HBox(10); // Horizontal layout with 10 pixels spacing
        root.setPadding(new Insets(20)); // Add padding for better appearance
        root.getChildren().addAll(fromButton, fromComboBox, toButton, toComboBox);

        // Create scene and set it on the stage
        Scene scene = new Scene(root, 400, 150);
        primaryStage.setScene(scene);

        // Show the stage
        primaryStage.show();
    }
}
